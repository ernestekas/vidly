﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
       
        public ActionResult Index()
        {
            var movies = _context.Movies.Include(m => m.Genre).ToList();
            return View(movies);
        }

        public ActionResult Details(int id)
        {// Kas yra singleOrDefault?
            var movie = _context.Movies.Include(m => m.Genre).SingleOrDefault(m => m.Id == id);
            
            if (movie == null)
                return HttpNotFound();
            
            return View(movie);
            
        }
        public ActionResult Random()
        {
            // Define movie variable using Movie Model
            var movie = new Movie() { Name = "Shrek!" };
            // Pass movie var to View
            return View(movie);
        }
    }
}