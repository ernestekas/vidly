﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
// Tapo nebereikalinga eilute, nes perduoti duomenis nebereikia iš metodo, o iš duomenų bazės.
// using Vidly.ViewModels;
using System.Data.Entity;


namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        // DB context to access database (L29)
        private ApplicationDbContext _context;

        // Constructor (L29)
        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        // this Db context is disposable object, so we need to properly dispose this object (L29). QQQ - why to dispose context?
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Customers
        // This controler must create List of customers and pass that info to View: /Customers/Index (/Customers)
        public ActionResult Index()
        {
            /* This works good, this list passed to view and view creates table with names in a list and ID's good.
             * But I need to use ID's from this method in another in this controller so I need to create this lisst in another method here so
             * I can use that same method where list is in more than one actionresults. 
            List<Customer> customers = new List<Customer>
            {
                new Customer {Id = 1, Name = "Jonas Bublikas" },
                new Customer {Id = 2, Name = "Petras Nejonas"}
            };*/
            // L29 - chanage
            //var customers = GetCustomers();

            // Čia metė klaidą (Cannot convert lambda expression to type string because it is not delegate type šioje vietoje: Include(c => c.MembershipType).
            // Klaidą ištaisė pridėjus: using System.Data.Entity
            // Rekomendacija taip pat patikrinti ar yra ši eilutė: using system.Linq

            //Ši eilutė papildoma L30. Tikslas į View perduoti ne tik vardą pavardę, bet ir Membership Type. Todėl rašoma Include ir tt.
            var customers = _context.Customers.Include(c => c.MembershipType).ToList();

            return View(customers);
            /* changed in L29
            var viewModel = new CustomersViewModels
            {
                Customers = customers
            };
            return View(viewModel);
            */
        }
        public ActionResult Details(int id)
        {

            //var customer = _context.Customers.SingleOrDefault(c => c.Id == id);
            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);

            /* var customers = GetCustomers();
            var customer = new Customer();

            if (customers == null)
            {
                return HttpNotFound();
            }
            else if(id <= customers.Count)
            {
                customer = customers[id - 1];
                return View(customer);
            }
            else
            {
                return PartialView("NoSuchCustomer");
            }
            */
            
            
        }
        /* L29 this is not needed anymore as we now are using database
        private List<Customer> GetCustomers()
        {
            return new List<Customer>
            {
                new Customer { Id = 1, Name = "Jonas Bublikas"},
                new Customer { Id = 2, Name = "Petras Nejonas"}
            };
        }
        */

    }
}