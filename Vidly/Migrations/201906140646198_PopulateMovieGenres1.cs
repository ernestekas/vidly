namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMovieGenres1 : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO MovieGenres (Id, Name) VALUES (2, 'Thriller')");
            Sql("INSERT INTO MovieGenres (Id, Name) VALUES (3, 'Action')");
            Sql("INSERT INTO MovieGenres (Id, Name) VALUES (4, 'Family')");
            Sql("INSERT INTO MovieGenres (Id, Name) VALUES (5, 'Drama')");
            Sql("INSERT INTO MovieGenres (Id, Name) VALUES (6, 'Adventure')");
            Sql("INSERT INTO MovieGenres (Id, Name) VALUES (7, 'Sci-Fi')");
        }
        
        public override void Down()
        {
        }
    }
}
