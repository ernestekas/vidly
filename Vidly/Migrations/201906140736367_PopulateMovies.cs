﻿namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMovies : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Liongino kelionės', 1990/05/02, 2019/06/14, 5, 1)");
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Kambario Tribunolas', 1999/08/01, 2019/06/14, 5, 2)");
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Bebras', 2014/05/02, 2019/06/14, 5, 3)");
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Liongino kelionės 2', 1995/05/02, 2019/06/14, 5, 4)");
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Lėti ir nurimę', 2012/05/02, 2019/06/14, 5, 5)");
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Traukinys', 2018/04/02, 2019/06/14, 5, 6)");
            Sql("INSERT INTO Movies (Name, ReleaseDay, DateAdded, NumberInStock, GenreId) VALUES ('Tutituti', 2015/05/02, 2019/06/14, 5, 7)");







        }

        public override void Down()
        {
        }
    }
}
