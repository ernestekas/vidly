namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeBirthDateNullableAtCustomer : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "BirthYear", c => c.Short());
            AlterColumn("dbo.Customers", "BirthMonth", c => c.Short());
            AlterColumn("dbo.Customers", "BirthDay", c => c.Short());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "BirthDay", c => c.Short(nullable: true));
            AlterColumn("dbo.Customers", "BirthMonth", c => c.Short(nullable: true));
            AlterColumn("dbo.Customers", "BirthYear", c => c.Short(nullable: true));
        }
    }
}
