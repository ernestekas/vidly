namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMoviesDatesId3 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Movies set ReleaseDay='2008-01-01', DateAdded='2019-06-14' Where Id=3");

        }

        public override void Down()
        {
        }
    }
}
