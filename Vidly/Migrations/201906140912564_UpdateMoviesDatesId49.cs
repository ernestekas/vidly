namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMoviesDatesId49 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Movies set ReleaseDay='1998-01-01', DateAdded='2019-06-14' Where Id=4");
            Sql("UPDATE Movies set ReleaseDay='2008-01-01', DateAdded='2019-06-14' Where Id=5");
            Sql("UPDATE Movies set ReleaseDay='1995-01-01', DateAdded='2019-06-14' Where Id=6");
            Sql("UPDATE Movies set ReleaseDay='2018-01-01', DateAdded='2019-06-14' Where Id=7");
            Sql("UPDATE Movies set ReleaseDay='2011-01-01', DateAdded='2019-06-14' Where Id=8");
            Sql("UPDATE Movies set ReleaseDay='2010-01-01', DateAdded='2019-06-14' Where Id=9");
        }

        public override void Down()
        {
        }
    }
}
