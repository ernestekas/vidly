namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealPopulateBirthDateInCustomers : DbMigration
    {
        public override void Up()
        {
            //Sql("INSERT INTO Customers(BirthYear, BirthMonth, BirthDay) VALUES (1991, 01, 01)");
            Sql("UPDATE Customers set BirthYear=1991, BirthMonth=1, BirthDay=1 Where Id=1");
        }
        
        public override void Down()
        {
        }
    }
}
