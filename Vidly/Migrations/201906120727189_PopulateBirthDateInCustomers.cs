﻿namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    // migration pavadinimas parinktas blogai. Pirmiausia turėjau pavadinti taip: add-migration AddBirthDateToCustomers
    public partial class PopulateBirthDateInCustomers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "BirthYear", c => c.Short(nullable: false));
            AddColumn("dbo.Customers", "BirthMonth", c => c.Short(nullable: false));
            AddColumn("dbo.Customers", "BirthDay", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "BirthDay");
            DropColumn("dbo.Customers", "BirthMonth");
            DropColumn("dbo.Customers", "BirthYear");
        }
    }
}
