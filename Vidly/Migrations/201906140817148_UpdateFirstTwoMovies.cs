﻿namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFirstTwoMovies : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Movies set Name='Lopytos kelnės', ReleaseDay='2008-01-01', DateAdded='2019-06-14', NumberInStock=5, GenreId=5 Where Id=1");
            Sql("UPDATE Movies set Name='Turbulencija', ReleaseDay='2008-01-01', DateAdded='2019-06-14', NumberInStock=5, GenreId=4 Where Id=2");

        }

        public override void Down()
        {
        }
    }
}
