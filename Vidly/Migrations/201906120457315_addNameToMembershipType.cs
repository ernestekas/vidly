﻿namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNameToMembershipType : DbMigration
    {
        /* Kadangi naudojau Code-First migracijas turėjau rašyti taip:
         * 1. sukurti tuščią migracija:
         PM> add-migration addNameToMembershipTypes
         Tada:
         public override void Up()
        {
            Sql("INSERT INTO MembershipTypes(Name) VALUES ("Pay as You Go")");
            Sql("INSERT INTO MembershipTypes(Name) VALUES ("Monthly")");
            Sql("INSERT INTO MembershipTypes(Name) VALUES ("Three Months")");
            Sql("INSERT INTO MembershipTypes(Name) VALUES ("Annual")");
        }
        */

        public override void Up()
        {
            AddColumn("dbo.MembershipTypes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MembershipTypes", "Name");
        }
    }
}
