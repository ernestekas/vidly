﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public MovieGenre Genre { get; set; }
        public byte GenreId { get; set; }
        public DateTime ReleaseDay { get; set; }
        public DateTime DateAdded { get; set; }
        public short NumberInStock { get; set; }
    }
}